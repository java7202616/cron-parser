### Simple cron expression parser

Commandline app for parsing cron expressions and expanding each field

#### Building

Execute `gradlew clean build jar` to build, run tests and create a runnable jar file

#### Execution

Execute `java -jar ./build/libs/cron-parser-1.0.jar` to run from command line (JRE 11 required)
or Execute `./cron-parser.sh` or `./cron-parser.cmd` which is a shortcut of the above command.

Application expects 1 argument in following format:
`"<minute> <hour> <day_of_month> <month> <day_of_week> <command>"` (including `""`)

#### Supported syntax:

- "/" interval (e.g. "0/5" or "*/5" for every fifth value)
- "-' range (e.g. "1-5" for all values between 1 and 5)
- "," list (e.g. "1,3,5")
- "*" wildcard (to specify all values for given field)
- "?" non-relevant field (available for <day_of_week> and <day_of_moth>)
- intervals, ranges and lists can be mixed together (e.g. "1,2-3,5/5")

