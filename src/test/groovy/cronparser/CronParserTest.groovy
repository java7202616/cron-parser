package cronparser


import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

import static cronparser.CronParser.TOOLTIP

class CronParserTest extends Specification {

    def "should print result in correct format"() {
        given:
        def outputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outputStream))

        when:
        CronParser.main("0/5 */2 1-31 1-5,7-11 ? /command command_arg")

        then:
        outputStream.toString().lines().collect(Collectors.toList()) == [
                "minute        0 5 10 15 20 25 30 35 40 45 50 55",
                "hour          0 2 4 6 8 10 12 14 16 18 20 22",
                "day of month  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31",
                "month         1 2 3 4 5 7 8 9 10 11",
                "day of week   ",
                "command       /command command_arg"
        ]
    }

    @Unroll
    def "should validate input args"(String[] args, message) {
        when:
        CronParser.prepareArgs(args)

        then:
        IllegalArgumentException exception = thrown()
        exception.message == message

        where:
        args                             || message
        ["* * * * *"]                    || "Invalid number of arguments. " + TOOLTIP
        ["* * * * ?"]                    || "Invalid number of arguments. " + TOOLTIP
        ["*", "*", "*", "*", "*", "cmd"] || "Invalid number of arguments. " + TOOLTIP
        ["* * 0 * 0 cmd"]                || "Both <day_of_month> and <day_of_week> cannot be provided at the same time. " + TOOLTIP
        ["* * * * 0 cmd"]                || "Both <day_of_month> and <day_of_week> cannot be provided at the same time. " + TOOLTIP
        ["* * 0 * * cmd"]                || "Both <day_of_month> and <day_of_week> cannot be provided at the same time. " + TOOLTIP
        ["* * ? * ? cmd"]                || "<day_of_month> or <day_of_week> needs to be specified. " + TOOLTIP
    }

}
