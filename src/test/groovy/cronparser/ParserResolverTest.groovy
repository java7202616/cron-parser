package cronparser

import cronparser.parsers.ConstantParser
import cronparser.parsers.IntervalParser
import cronparser.parsers.ListParser
import cronparser.parsers.RangeParser
import spock.lang.Specification
import spock.lang.Unroll

class ParserResolverTest extends Specification {

    @Unroll
    def "should resolve parser #parser by expression #expression"(expression, parser) {
        expect:
        ParserResolver.getParserForString(expression).class == parser

        where:
        expression || parser
        "0"        || ConstantParser
        "*"        || ConstantParser
        "?"        || ConstantParser
        "999"      || ConstantParser
        "0-0"      || RangeParser
        "0-999"    || RangeParser
        "1,2"      || ListParser
        "1,2,3"    || ListParser
        "1,2/3"    || ListParser
        "1,2/3"    || ListParser
        "2/3"      || IntervalParser
    }

    @Unroll
    def "should fail resolving parser by expression #expression"(expression) {
        when:
        ParserResolver.getParserForString(expression)

        then:
        IllegalArgumentException exception = thrown()
        exception.message == "Unsupported syntax: $expression"

        where:
        expression << [
                "a",
                "-1",
                "1-1-1",
                "1/1/1",
                "**",
                "??",
                "?/1",
                "1/?",
                "1/*",
                "1/-1",
                "1/a",
        ]

    }

}
