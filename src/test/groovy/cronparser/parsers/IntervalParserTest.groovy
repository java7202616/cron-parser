package cronparser.parsers

import cronparser.ParserContext
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

class IntervalParserTest extends Specification {

    private IntervalParser intervalParser

    def setup() {
        intervalParser = new IntervalParser()
    }

    @Unroll
    def "should parse valid interval #input and return #expectedValues"(input, expectedValues) {
        when:
        def result = intervalParser.parse(input, ParserContext.create(0, 12))

        then:
        result.boxed().collect(Collectors.toSet()) == expectedValues as Set

        where:
        input || expectedValues
        "0/3" || [0, 3, 6, 9, 12]
        "0/5" || [0, 5, 10]
        "1/5" || [1, 6, 11]
        "5/5" || [5, 10]
        "3/3" || [3, 6, 9, 12]
        "7/6" || [7]
        "*/2" || [0, 2, 4, 6, 8, 10, 12]
    }

    @Unroll
    def "should fail parsing invalid interval #input with #message"(input, message, minValue, maxValue) {
        when:
        intervalParser.parse(input, ParserContext.create(minValue, maxValue))

        then:
        IllegalArgumentException exception = thrown()
        exception.message == message

        where:
        input  | minValue | maxValue || message
        "0/1"  | 1        | 10       || "Invalid expression: $input. Expected values from range: $minValue-$maxValue"
        "1/11" | 1        | 10       || "Invalid expression: $input. Expected values from range: $minValue-$maxValue"
    }

}
