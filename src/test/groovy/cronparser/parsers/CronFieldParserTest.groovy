package cronparser.parsers

import cronparser.ParserContext
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

class CronFieldParserTest extends Specification {

    private CronFieldParser parser

    def setup() {
        parser = new CronFieldParser()
    }

    @Unroll
    def "should parse expression #expression"(expression, expectedValues) {
        when:
        def result = parser.parse(expression, ParserContext.create(0, 100))

        then:
        result.boxed().collect(Collectors.toSet()) == expectedValues as Set

        where:
        expression   || expectedValues
        "2-5"        || [2, 3, 4, 5]
        "55"         || [55]
        "1,5-7"      || [1, 5, 6, 7]
        "5-7,10"     || [5, 6, 7, 10]
        "5-7,9-10"   || [5, 6, 7, 9, 10]
        "1,5-7,9-10" || [1, 5, 6, 7, 9, 10]
        "0/25,3"     || [0, 3, 25, 50, 75, 100]
        "*/25,3"     || [0, 3, 25, 50, 75, 100]
    }

    @Unroll
    def "should fail to parse expression '#input' with message '#message'"(input, message) {
        when:
        parser.parse(input, ParserContext.create(0, 10))
                .boxed().collect(Collectors.toList())
        then:
        IllegalArgumentException exception = thrown()
        exception.message == message

        where:
        input   || message
        "-2-5"  || "Unsupported syntax: $input"
        "2-5-"  || "Unsupported syntax: $input"
        "2.1-5" || "Unsupported syntax: $input"
        "A"     || "Unsupported syntax: $input"
        "1/2/3" || "Unsupported syntax: $input"
        "**"    || "Unsupported syntax: $input"
        "*,*"   || "Wildcard not allowed: *"
        "*,1"   || "Wildcard not allowed: *"
    }

}
