package cronparser.parsers

import cronparser.ParserContext
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

class ConstantParserTest extends Specification {

    private ConstantParser constantParser

    def setup() {
        constantParser = new ConstantParser()
    }

    @Unroll
    def "should parse valid constant #input and return #expectedValues"(input, expectedValues) {
        when:
        def result = constantParser.parse(input, ParserContext.create(0, 3, true, true))

        then:
        result.boxed().collect(Collectors.toSet()) == expectedValues as Set

        where:
        input || expectedValues
        "0"   || [0]
        "3"   || [3]
        "*"   || [0, 1, 2, 3]
        "?"   || []
    }

    @Unroll
    def "should fail parsing invalid constant #input with #message"(input, message, minValue, maxValue) {
        when:
        constantParser.parse(input, ParserContext.create(minValue, maxValue, true, false))

        then:
        IllegalArgumentException exception = thrown()
        exception.message == message

        where:
        input | minValue | maxValue || message
        "11"  | 1        | 10       || "Invalid value: $input. Expected value from range: $minValue-$maxValue"
        "?"   | 1        | 10       || "'?' is not allowed for this field"
    }
}
