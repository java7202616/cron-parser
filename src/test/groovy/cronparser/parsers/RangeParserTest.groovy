package cronparser.parsers

import cronparser.ParserContext
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Collectors

class RangeParserTest extends Specification {

    private RangeParser rangeParser

    def setup() {
        rangeParser = new RangeParser()
    }

    @Unroll
    def "should parse valid range #input and return #expectedValues"(input, expectedValues) {
        when:
        def result = rangeParser.parse(input, ParserContext.create(0, 3))

        then:
        result.boxed().collect(Collectors.toSet()) == expectedValues as Set

        where:
        input || expectedValues
        "0-3" || [0, 1, 2, 3]
        "1-1" || [1]
    }

    @Unroll
    def "should fail parsing invalid range #input with #message"(input, message, minValue, maxValue) {
        when:
        rangeParser.parse(input, ParserContext.create(minValue, maxValue))

        then:
        IllegalArgumentException exception = thrown()
        exception.message == message

        where:
        input  | minValue | maxValue || message
        "2-1"  | 1        | 10       || "Invalid range: $input"
        "0-1"  | 1        | 10       || "Invalid range: $input. Expected values from range: $minValue-$maxValue"
        "1-11" | 1        | 10       || "Invalid range: $input. Expected values from range: $minValue-$maxValue"
    }

}
