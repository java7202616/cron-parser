package cronparser;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CronField {
    MINUTES(0, "minute", ParserContext.create(0, 59)),
    HOURS(1, "hour", ParserContext.create(0, 23)),
    DAYS_OF_MONTH(2, "day of month", ParserContext.create(1, 31, true, true)),
    MONTHS(3, "month", ParserContext.create(1, 12)),
    DAYS_OF_WEEK(4, "day of week", ParserContext.create(0, 6, true, true));

    private final int index;
    private final String label;
    private final ParserContext context;
}
