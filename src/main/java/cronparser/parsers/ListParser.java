package cronparser.parsers;

import cronparser.ParserContext;
import cronparser.ParserResolver;

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ListParser implements Parser {

    @Override
    public IntStream parse(String s, ParserContext context) {
        String[] tokens = s.split(",");
        return Stream.of(tokens)
                .map(token -> ParserResolver.getParserForString(token)
                        .parse(token, ParserContext.create(context.getMinValue(), context.getMaxValue(), false, context.isQuestionMarkAllowed())))
                .flatMapToInt(Function.identity());
    }
}
