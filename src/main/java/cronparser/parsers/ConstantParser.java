package cronparser.parsers;

import cronparser.ParserContext;

import java.util.stream.IntStream;

public class ConstantParser implements Parser {

    @Override
    public IntStream parse(String s, ParserContext context) {
        if ("*".equals(s)) {
            if (context.isWildcardAllowed()) {
                return IntStream.rangeClosed(context.getMinValue(), context.getMaxValue());
            } else {
                throw new IllegalArgumentException("Wildcard not allowed: " + s);
            }
        }
        if ("?".equals(s)) {
            if (context.isQuestionMarkAllowed()) {
                return IntStream.empty();
            } else {
                throw new IllegalArgumentException("'?' is not allowed for this field");
            }
        }
        int value = Integer.parseInt(s);
        if (value < context.getMinValue() || value > context.getMaxValue()) {
            throw new IllegalArgumentException(String.format("Invalid value: %s. Expected value from range: %d-%d", s, context.getMinValue(), context.getMaxValue()));
        }
        return IntStream.rangeClosed(value, value);
    }
}
