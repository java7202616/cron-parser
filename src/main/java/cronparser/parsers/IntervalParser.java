package cronparser.parsers;

import cronparser.ParserContext;

import java.util.stream.IntStream;

public class IntervalParser implements Parser {

    @Override
    public IntStream parse(String s, ParserContext context) {
        String[] tokens = s.split("/");
        int base = tokens[0].equals("*") ? 0 : Integer.parseInt(tokens[0]);
        int increment = Integer.parseInt(tokens[1]);

        if (base < context.getMinValue() || increment > context.getMaxValue()) {
            throw new IllegalArgumentException(String.format("Invalid expression: %s. Expected values from range: %d-%d", s, context.getMinValue(), context.getMaxValue()));
        }

        return IntStream.rangeClosed(0, (context.getMaxValue() - base) / increment).map(i -> base + i * increment);
    }
}
