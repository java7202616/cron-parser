package cronparser.parsers;

import cronparser.ParserContext;

import java.util.stream.IntStream;

public class RangeParser implements Parser {

    public IntStream parse(String s, ParserContext context) {
        String[] tokens = s.split("-");
        int lowerValue = Integer.parseInt(tokens[0]);
        int upperValue = Integer.parseInt(tokens[1]);
        if (lowerValue > upperValue) {
            throw new IllegalArgumentException("Invalid range: " + s);
        }
        if (lowerValue < context.getMinValue() || upperValue > context.getMaxValue()) {
            throw new IllegalArgumentException(String.format("Invalid range: %s. Expected values from range: %d-%d", s, context.getMinValue(), context.getMaxValue()));
        }
        return IntStream.rangeClosed(lowerValue, upperValue);
    }

}
