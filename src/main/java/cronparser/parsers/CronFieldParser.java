package cronparser.parsers;

import cronparser.ParserContext;
import cronparser.ParserResolver;

import java.util.stream.IntStream;

public class CronFieldParser implements Parser {

    @Override
    public IntStream parse(String s, ParserContext context) {
        return ParserResolver.getParserForString(s).parse(s, context);
    }
}
