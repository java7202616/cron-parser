package cronparser.parsers;

import cronparser.ParserContext;

import java.util.stream.IntStream;

public interface Parser {

    IntStream parse(String s, ParserContext context);

}
