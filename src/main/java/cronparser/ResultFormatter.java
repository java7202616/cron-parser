package cronparser;

import lombok.experimental.UtilityClass;

import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UtilityClass
public class ResultFormatter {

    public static String format(String fieldName, IntStream values) {
        return String.format("%-14s%s", fieldName, values.boxed().collect(Collectors.toCollection(TreeSet::new))
                .stream().map(Object::toString).collect(Collectors.joining(" ")));
    }

    public static String format(String fieldName, String[] values) {
        return String.format("%-14s%s", fieldName, String.join(" ", values));
    }

}
