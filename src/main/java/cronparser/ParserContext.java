package cronparser;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ParserContext {

    private final int minValue;
    private final int maxValue;
    private final boolean isWildcardAllowed;
    private final boolean isQuestionMarkAllowed;

    public static ParserContext create(int minValue, int maxValue) {
        return new ParserContext(minValue, maxValue, true, false);
    }

    public static ParserContext create(int minValue, int maxValue, boolean allowWildcard, boolean isQuestionMarkAllowed) {
        return new ParserContext(minValue, maxValue, allowWildcard, isQuestionMarkAllowed);
    }
}
