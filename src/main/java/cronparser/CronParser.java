package cronparser;

import cronparser.parsers.CronFieldParser;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CronParser {

    static final String TOOLTIP = "Usage: cron-parse \"<minute> <hour> <day_of_month> <month> <day_of_week> <command>\"";

    public static void main(String[] args) {
        try {
            String[] input = prepareArgs(args);
            CronFieldParser fieldParser = new CronFieldParser();
            String result = Stream.of(CronField.values())
                    .map(f -> {
                        try {
                            return ResultFormatter.format(f.getLabel(), fieldParser.parse(input[f.getIndex()], f.getContext()));
                        } catch (IllegalArgumentException e) {
                            System.err.printf("Error parsing field '%s'. %s%n", f.getLabel(), e.getMessage());
                            System.exit(1);
                            return null;
                        }
                    }).collect(Collectors.joining("\n"));
            result += "\n" + ResultFormatter.format("command", Arrays.copyOfRange(input, 5, input.length));
            System.out.println(result);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    static String[] prepareArgs(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Invalid number of arguments. " + TOOLTIP);
        }
        String[] cronArgs = args[0].split(" ");
        if (cronArgs.length < 6) {
            throw new IllegalArgumentException("Invalid number of arguments. " + TOOLTIP);
        }
        if (!"?".equals(cronArgs[CronField.DAYS_OF_MONTH.getIndex()]) && !"?".equals(cronArgs[CronField.DAYS_OF_WEEK.getIndex()])) {
            throw new IllegalArgumentException("Both <day_of_month> and <day_of_week> cannot be provided at the same time. " + TOOLTIP);
        }
        if ("?".equals(cronArgs[CronField.DAYS_OF_MONTH.getIndex()]) && "?".equals(cronArgs[CronField.DAYS_OF_WEEK.getIndex()])) {
            throw new IllegalArgumentException("<day_of_month> or <day_of_week> needs to be specified. " + TOOLTIP);
        }
        return cronArgs;
    }

}
