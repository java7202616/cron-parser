package cronparser;

import cronparser.parsers.*;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ParserFactory {

    public static Parser getListParser() {
        return new ListParser();
    }

    public static Parser getConstantParser() {
        return new ConstantParser();
    }

    public static Parser getRangeParser() {
        return new RangeParser();
    }

    public static Parser getIntervalParser() {
        return new IntervalParser();
    }
}
