package cronparser;

import cronparser.parsers.Parser;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ParserResolver {

    public static Parser getParserForString(String string) {
        if (string.contains(",")) {
            return ParserFactory.getListParser();
        } else if (string.matches("(\\*|\\d+)/\\d+")) {
            return ParserFactory.getIntervalParser();
        } else if (string.matches("\\d+-\\d+")) {
            return ParserFactory.getRangeParser();
        } else if (string.matches("\\*|\\d+|\\?")) {
            return ParserFactory.getConstantParser();
        }
        throw new IllegalArgumentException("Unsupported syntax: " + string);
    }

}
